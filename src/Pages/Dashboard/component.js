import React, { Suspense} from "react";
import { Marker, Popup, GeoJSON, FeatureGroup, } from 'react-leaflet';
import  {RollbackOutlined } from '@ant-design/icons';
import { Select, DatePicker, Modal, Slider, Tooltip } from "antd";
import { useState } from "react";
import hot from "../../assets/hot.png";
import humidity from "../../assets/humidity.png";
import storm from "../../assets/storm.png";
import rain from "../../assets/rainfall.png";
import { Tabs, Carousel } from 'antd';


import axios from "axios";
import moment from "moment";
import { gresikdata } from "../../assets/Gresik";
import { dataMalang } from "../../assets/Malang";
import { dataSidoarjo } from "../../assets/Sidoarjo";
import { dataNunukan } from "../../assets/Nunukan";
import { kotawaringinbarat } from "../../assets/KotaWarbar";
import { Inhu } from "../../assets/inhu";
import { Tuban } from "../../assets/Tuban";
import { Kerinci } from "../../assets/kerinci";
import { Pontianak } from "../../assets/pontianak";

import { Spin } from "antd";
import { isEmpty } from "lodash"
import clsx from "clsx";
moment.locale('id')

const Chart = React.lazy(() => import("../../Component/Chart"));
const Chart2 = React.lazy(() => import("../../Component/Chart2"));
const BarChartV2 = React.lazy(() => import("../../Component/BarChartV2"));
const ChartMultiline = React.lazy(() => import("../../Component/BarChart"));
const Card = React.lazy(() => import("../../Component/Card"));
const MapGis = React.lazy(() => import("../../Component/Map"));


function Dashboard(props) {
  const [state, setState] = useState({
    province: null,
    kabupaten: null,
    stasiun: null,
    startDate: null,
    endDate: null,
    isModalVisible: false,
    dataPrediksi: null,
    dataForecast: null,
    error: null,
    parameter: null,
    fetching: false,
    slider: 0,
    showDropDown: true,
    parameterName: null,
    forecast: false
  })
  const { startDate, endDate, fetching, slider, showDropDown } = state;
  const handlePrediksi = () => {
    setState({ ...state, fetching: true })
    axios.get(`https://tio.app.adaptivenetlab.site/api?sheetName=${kabupaten}&worksheetName=Data%20Harian%20-%20Table&periods=10&start=${moment(startDate).format("YYYY-MM-DD")}&end=${moment(endDate).format("YYYY-MM-DD")}`)
      .then((res) => {

        setState({
          ...state, dataPrediksi: res.data, fetching: false, showDropDown: false, forecast: false 
        })
      })
      .catch((err) => {
        setState({ ...state, error: err, fetching: false, showDropDown: false, forecast:false })
      })
  }
  const handleForecast = () => {
    setState({ ...state, fetching: true })
    axios.get(`https://tio.app.adaptivenetlab.site/api/forecast?sheetName=${kabupaten}&worksheetName=Data%20Harian%20-%20Table&periods=10&fore=${slider}`)
      .then((res) => {
        setState({ ...state, dataPrediksi: res.data, fetching: false, showDropDown: false, forecast:true })
      })
      .catch((err) => {
        setState({ ...state, error: err, fetching: false, showDropDown: false, forecast: true }) 
      })
  }
  const { province, kabupaten, stasiun,  isModalVisible } = state
  const handleOk = () => {
    setState({ ...state, isModalVisible: !isModalVisible })
  } 
  const handleCancel = () => {
    setState({ ...state, isModalVisible: !isModalVisible, parameter: null })
  }
  const ModalParameters = () => {
    console.log(state.parameterName,"tesd")
    return (
      <Suspense fallback={<Spin />}>
        <Modal title={state.parameterName} open={isModalVisible} onOk={handleOk} onCancel={handleCancel} footer={false}>
          <Chart data={state.parameter} tanggal={tanggal[0]} labels="parameter"/>
        </Modal>
      </Suspense>
    )
  }
  const Provinsi = [
    { value: 0, label: 'Kalimantan Utara' },
    { value: 1, label: 'Kalimantan Tengah' },
    { value: 3, label: 'Jawa Timur' },
    { value: 4, label: 'Riau' },
    { value: 5, label: 'Jambi' },
    { value: 6, label: 'Kalimantan Barat' },
  ]
  

  const Kabupaten = [
    { value: 'nunukanNew', label: 'Kab. Nunukan', id_province: 0 , id_kabupaten: 0},
    { value: 'kotawabar', label: 'Kab. Kotawaringin Barat', id_province: 1, id_kabupaten: 1 },
    { value: 'malangBaru', label: 'Kab. Malang' , id_province: 3, id_kabupaten: 2},
    { value: 'gresikBaru', label: 'Kab. Gresik' , id_province: 3, id_kabupaten: 3},
    { value: 'sidoarjoNew', label: 'Kab. Sidoarjo', id_province: 3, id_kabupaten: 4 },
    { value: 'indragiri hulu', label: 'Kab. Indragiri Hulu', id_province: 4, id_kabupaten: 6 },
    { value: 'kab tuban', label: 'Kab. Tuban', id_province: 3, id_kabupaten: 9 },
    { value: 'kerinciNew', label: 'Kab. Kerinci', id_province: 5, id_kabupaten: 10 },
    { value:'Kota pontianak', label: 'Kota Pontianak', id_province: 6, id_kabupaten: 11 },
  ]
  let filterKabupaten = Kabupaten.filter((item) => {
    return item.id_province === province
  })
  const Stasiun = [
    { value: 'Stasiun Meterologi Nunukan', label: 'Stasiun Meterologi Nunukan', nama_kabupaten: "nunukanNew" },
    { value: 'Stasiun Meterologi Iskandar', label: 'Stasiun Meterologi Iskandar', nama_kabupaten: "kotawabar"},
    { value: 'Stasiun Klimatologi Jawa Timur', label: 'Stasiun Klimatologi Jawa Timur', 
      nama_kabupaten: "malangBaru" },
    { value: 'Stasiun Meteorologi Sangkapura', label: 'Stasiun Meteorologi Sangkapura', 
      nama_kabupaten: "gresikBaru" },
    {
      value: 'Stasiun Meteorologi Juanda', label: 'Stasiun Meteorologi Juanda',
      nama_kabupaten: "sidoarjoNew"
    },
    { value: 'Stasiun Meteorologi Japura', label: 'Stasiun Meteorologi Japura', nama_kabupaten: "indragiri hulu" },
    { value: 'Stasiun Meteorologi Tuban', label: 'Stasiun Meteorologi Tuban', nama_kabupaten: "kab tuban" },
    { value: 'Stasiun Meteorologi Depati Parbo', label: 'Stasiun Meteorologi Depati Parbo', nama_kabupaten: "kerinciNew" },
    { value: 'Stasiun Meteorologi Maritim ', label: 'Stasiun Meteorologi Maritim', nama_kabupaten: "Kota pontianak" },
  ]

  let filterStasiun = Stasiun.filter((item) => {
    return item.nama_kabupaten === kabupaten
  })
  
  let tempPrediksi = []
  let humPrediksi = []
  let windPrediksi = []
  let rainPrediksi = []
  tempPrediksi = state.dataPrediksi?.Data_Result?.map((item) => {
    return item?.Prediction?.Temp
  })
  humPrediksi = state.dataPrediksi?.Data_Result?.map((item) => {
    return item?.Prediction?.Humidity
  })
  windPrediksi = state.dataPrediksi?.Data_Result?.map((item) => {
    return item?.Prediction?.Wind
  })
  rainPrediksi = state.dataPrediksi?.Data_Result?.map((item) => {
    return item?.Prediction?.Rainfall
  })
  const listDataParameter = [
    {
      title:"Temperature",
      subtitle:!isEmpty(tempPrediksi)&& `${tempPrediksi[tempPrediksi?.length - 1]}°C` ,
      icon:hot,
      data: tempPrediksi
    },
    {
      title:"Humidity",
      subtitle:!isEmpty(humPrediksi)&& `${humPrediksi[humPrediksi?.length - 1]}%` ,
      icon: humidity,
      data: humPrediksi
        
    },
    {
      title:"Wind",
      subtitle:!isEmpty(windPrediksi)&& `${windPrediksi[windPrediksi?.length - 1]}m/s` ,
      icon: storm,
      data: windPrediksi

    },
    {
      title:"Rainfall",
      subtitle:!isEmpty(rainPrediksi)&& `${rainPrediksi[rainPrediksi?.length - 1]}mm` ,
      icon: rain,
      data: rainPrediksi
    },
  ]
  const rendah = [];
  const tanggal = [];
  const sedang = [];
  const tinggi = [];
  const sangatTinggi = [];
  const fwi = [];
  const bui = [];
  const dc = [];
  const isi = [];
  const dmc = [];
  const ffmc = [];
  const error = state.dataPrediksi?.Prediksi_Error
  console.log(typeof state.dataPrediksi)
  ffmc.push(state.dataPrediksi?.Data_Result?.map((item) => {
    return item.Result?.Category?.ffmc
  }))
  bui.push(state.dataPrediksi?.Data_Result?.map((item) => {
    return item.Result?.Category?.bui
  }))
  dc.push(state.dataPrediksi?.Data_Result?.map((item) => {
    return item.Result?.Category?.dc
  }))
  isi.push(state.dataPrediksi?.Data_Result?.map((item) => {
    return item.Result?.Category?.isi
  }))
  dmc.push(state.dataPrediksi?.Data_Result?.map((item) => {
    return item.Result?.Category?.dmc
  }))

  fwi.push(state.dataPrediksi?.Data_Result?.map((item) => {
    return item.Result?.Category?.fwi
  }))
  console.log(state.dataPrediksi)
  sedang.push(state.dataPrediksi?.Data_Result?.map((item) => {
    if (item.Result?.Category?.fwi > 1 && item.Result?.Category?.fwi <= 6){
      return item.Result?.Category?.fwi
  }
  }))
  tinggi.push(state.dataPrediksi?.Data_Result?.map((item) => {
    if (item.Result?.Category?.fwi > 6 && item.Result?.Category?.fwi <= 13) {
      return item.Result?.Category?.fwi
    }
  }))
  sangatTinggi.push(state.dataPrediksi?.Data_Result?.map((item) => {
    if (item.Result?.Category?.fwi > 13) {
      return item.Result?.Category?.fwi
    }
  }))
  rendah.push(state.dataPrediksi?.Data_Result?.map((item) => {
    if (item.Result?.Category?.fwi <= 1) {
      return item.Result?.Category?.fwi
    }
  }))
  tanggal.push(state.dataPrediksi?.Data_Result?.map((item) => {
    return moment(item.Date).format("DD MMMM YYYY")
  }))

  const handleClickItemParameter = (value) => {
    setState({
      ...state, isModalVisible: !isModalVisible, parameter: value.data, parameterName: value.title
    })
  }
  const handleClikToHome = () => {
    window.location.href = "/"
  }
  const handleChangeDate = (dates, dateStrings) => {
    setState({
      ...state, startDate: dateStrings[0], endDate: dateStrings[1]
    })
  }

  const handleStarDate = (date, dateString) => {
    setState({
      ...state, startDate: dateString
    })

  }

  const handleEndDate = (date, dateString) => {
    setState({
      ...state, endDate: dateString
    })
  }

  const renderPoligon = () => {
    let ST = !isEmpty(sangatTinggi[0]) && sangatTinggi[0].filter((item) => {
      return item !== undefined
    })
    let T = !isEmpty(tinggi[0]) && tinggi[0].filter((item) => {
      return item !== undefined
    })

    let S = !isEmpty(sedang[0]) && sedang[0].filter((item) => {
      return item !== undefined
    })
    let R = !isEmpty(rendah[0]) && rendah[0].filter((item) => {
      return item !== undefined
    })
    let className = ""
    if (ST.length > 0) {
      className = "merah"
    } else if (T.length > 0) {
      className = "kuning"
    } else if (S.length > 0) {
      className = "hijau"
    } else if (R.length > 0) {
      className = "biru"
    } else {
      className="transparan"
    }
    switch (kabupaten) {
      case "nunukanNew":
        return (
          <GeoJSON data={dataNunukan} className={clsx('biru',{[className]: className})} />
        )
      case "kotawabar":
        return (
          <GeoJSON data={kotawaringinbarat} className={clsx('biru',{[className]: className})} />
        )
      case "malangBaru":
        return (
          <GeoJSON data={dataMalang} className={clsx('biru',{[className]: className})} />
        )
      case "gresikBaru":
        return (
          <GeoJSON data={gresikdata} className={clsx('biru',{[className]: className})} />
        )
      case "sidoarjoNew":
        return (
          <GeoJSON data={dataSidoarjo} className={clsx('biru',{[className]: className})} />
        )
      case "kab tuban":
        return (
          <GeoJSON data={Tuban} className={clsx('biru', { [className]: className })} />
        )
      case "indragiri hulu":
        return (
          <GeoJSON data={Inhu} className={clsx('biru', { [className]: className })} />
        )
      case "kerinciNew":
        return (
          <GeoJSON data={Kerinci} className={clsx('biru', { [className]: className })} />
        )
      case "Kota pontianak":
        return (
          <GeoJSON data={Pontianak} className={clsx('biru', { [className]: className })} />
        )
      default:
        break;
    }
  }

  const renderMarker = () => {
  


    const fwiTerbesar = !isEmpty(fwi[0]) && Math.max(...fwi[0])
    
    const date = fwiTerbesar && tanggal[0][fwi[0].indexOf(fwiTerbesar)]
    const buiTerbesar = !isEmpty(bui[0]) && bui[0][fwi[0].indexOf(fwiTerbesar)]
    const dcTerbesar = !isEmpty(dc[0]) && dc[0][fwi[0].indexOf(fwiTerbesar)]
    const isiTerbesar = !isEmpty(isi[0]) && isi[0][fwi[0].indexOf(fwiTerbesar)]
    const dmcTerbesar = !isEmpty(dmc[0]) && dmc[0][fwi[0].indexOf(fwiTerbesar)]
    const ffmcTerbesar = !isEmpty(ffmc[0]) && ffmc[0][fwi[0].indexOf(fwiTerbesar)]
    switch (kabupaten) {
      case "nunukanNew":
        return (
          <Marker position={[4.130709639832849, 117.66305415403026]} >
            <Popup>
              <span>Stasiun Meteorologi Nunukan</span>
              <hr />
              <span>Tanggal: {date}</span>
              <br />
              <span>FWI: {fwiTerbesar}</span>
              <br />
              <span>BUI: {buiTerbesar}</span>
              <br />
              <span>DC: {dcTerbesar}</span>
              <br />
              <span>ISI: {isiTerbesar}</span>
              <br />
              <span>DMC: {dmcTerbesar}</span>
              <br />
              <span>FFMC:{ffmcTerbesar}</span>
            </Popup>
          </Marker>
        )
      case "kotawabar":
        return (
          <Marker position={[-2.75, 111.5]}  >
            <Popup>
              <span>Stasiun Meteorologi Kotawaringin Barat</span>
              <hr />
              {/* <span>Status: {status}</span> */}
              <br />
              <span>Tanggal: {date}</span>
              <br />
              <span>FWI: {fwiTerbesar}</span>
              <br />
              <span>BUI: {buiTerbesar}</span>
              <br />
              <span>DC: {dcTerbesar}</span>
              <br />
              <span>ISI: {isiTerbesar}</span>
              <br />
              <span>DMC: {dmcTerbesar}</span>
              <br />
              <span>FFMC:{ffmcTerbesar}</span>
            </Popup>
          </Marker>
        )
      case "malangBaru":
        return (
          <Marker position={[-7.901117, 112.597553]}  >
            <Popup>
              <span>Stasiun Meteorologi Malang</span>
              <hr />
              {/* <span>Status: {status}</span> */}
              <br />
              <span>Tanggal: {date}</span>
              <br />
              <span>FWI: {fwiTerbesar}</span>
              <br />
              <span>BUI: {buiTerbesar}</span>
              <br />
              <span>DC: {dcTerbesar}</span>
              <br />
              <span>ISI: {isiTerbesar}</span>
              <br />
              <span>DMC: {dmcTerbesar}</span>
              <br />
              <span>FFMC:{ffmcTerbesar}</span>
            </Popup>
          </Marker>

        )
      case "gresikBaru":
        return (
          <Marker position={[-7.155297757970725, 112.65722872576013]}  >
            <Popup>
              <span>Stasiun Meteorologi Gresik</span>
              <hr />
              <br />
              <span>Tanggal: {date}</span>
              <br />
              <span>FWI: {fwiTerbesar}</span>
              <br />
              <span>BUI: {buiTerbesar}</span>
              <br />
              <span>DC: {dcTerbesar}</span>
              <br />
              <span>ISI: {isiTerbesar}</span>
              <br />
              <span>DMC: {dmcTerbesar}</span>
              <br />
              <span>FFMC:{ffmcTerbesar}</span>
            </Popup>
          </Marker>

        )
      case "sidoarjoNew":
        return (
          <Marker position={[-7.372563565, 112.78204515]} >
            <Popup>
              <span>Stasiun Meteorologi Sidoarjo</span>
              <hr />
              <br />
              <span>Tanggal: {date}</span>
              <br />
              <span>FWI: {fwiTerbesar}</span>
              <br />
              <span>BUI: {buiTerbesar}</span>
              <br />
              <span>DC: {dcTerbesar}</span>
              <br />
              <span>ISI: {isiTerbesar}</span>
              <br />
              <span>DMC: {dmcTerbesar}</span>
              <br />
              <span>FFMC:{ffmcTerbesar}</span>
            </Popup>
          </Marker>
        )
      case "kab tuban":
        return (
          <Marker position={[-6.9, 112.05]} >
            <Popup>
              <span>Stasiun Meteorologi Tuban</span>
              <hr />
              {/* <span>Status: {status}</span> */}
              <br />
              <span>Tanggal: {date}</span>
              <br />
              <span>FWI: {fwiTerbesar}</span>
              <br />
              <span>BUI: {buiTerbesar}</span>
              <br />
              <span>DC: {dcTerbesar}</span>
              <br />
              <span>ISI: {isiTerbesar}</span>
              <br />
              <span>DMC: {dmcTerbesar}</span>
              <br />
              <span>FFMC:{ffmcTerbesar}</span>
            </Popup>
          </Marker>
        )
      case "indragiri hulu":
        return (
          <Marker position={[-0.34912606269587826, 102.33398245501206]} >
            <Popup>
              <span>Stasiun Meteorologi Ingragiri Hulu</span>
              <hr />
              {/* <span>Status: {status}</span> */}
              <br />
              <span>Tanggal: {date}</span>
              <br />
              <span>FWI: {fwiTerbesar}</span>
              <br />
              <span>BUI: {buiTerbesar}</span>
              <br />
              <span>DC: {dcTerbesar}</span>
              <br />
              <span>ISI: {isiTerbesar}</span>
              <br />
              <span>DMC: {dmcTerbesar}</span>
              <br />
              <span>FFMC:{ffmcTerbesar}</span>
            </Popup>
          </Marker>
        )
      case "kerinciNew": {
        return (
          <Marker position={[-2.090888760408868, 101.46197119553469]} >
            <Popup>
              <span>Stasiun Meteorologi Kerinci</span>
              <hr />
              {/* <span>Status: {status}</span> */}
              <br />
              <span>Tanggal: {date}</span>
              <br />
              <span>FWI: {fwiTerbesar}</span>
              <br />
              <span>BUI: {buiTerbesar}</span>
              <br />
              <span>DC: {dcTerbesar}</span>
              <br />
              <span>ISI: {isiTerbesar}</span>
              <br />
              <span>DMC: {dmcTerbesar}</span>
              <br />
              <span>FFMC:{ffmcTerbesar}</span>
            </Popup>
          </Marker>
        )
      }
      case "Kota pontianak": {
        return (
          <Marker position={[-0.020192421350998684, 109.3377432278057]} >
            <Popup>
              <span>Stasiun Meteorologi Pontianak</span>
              <hr />
              {/* <span>Status: {status}</span> */}
              <br />
              <span>Tanggal: {date}</span>
              <br />
              <span>FWI: {fwiTerbesar}</span>
              <br />
              <span>BUI: {buiTerbesar}</span>
              <br />
              <span>DC: {dcTerbesar}</span>
              <br />
              <span>ISI: {isiTerbesar}</span>
              <br />
              <span>DMC: {dmcTerbesar}</span>
              <br />
              <span>FFMC:{ffmcTerbesar}</span>
            </Popup>
          </Marker>
        )
      }
      default:
        break;
    }
  }
  const renderCenter = () => {

    switch (kabupaten) {
      case "nunukanNew":
        return [4.134346706869226, 116.96281655592071]
      case "kotawabar":
        return [-2.3030999211714436, 111.84655464398712]
      case "malangBaru":
        return [-7.9585767414524025, 112.63486798709273]
      case "SidoarjoNew":
        return [-7.43836493077115, 112.67932707331948]
      case "kab tuban":
        return [-6.889701117784201, 112.03479763466872]
      case "indragiri hulu":
        return [-0.4018912729533931, 102.16298956961433]
      case "kerinci":
        return [-0.4018912729533931, 102.16298956961433]
      default:
        return [-1.989942, 111.619572]
    }
  }
  let disabled = province === null || kabupaten === null || stasiun === null || startDate === null || endDate === null
  let disabledForcast = province === null || kabupaten === null || stasiun === null || state.slider === 0
  
  const handleDisableDate = (date) => {

    return date > moment("2023-06-03") || date < moment("2020-01-01")
  }
  const handleChangeSlider = (value) => {
    setState({
      ...state,
      slider: value
    })
  }

  const handleSesudahStartDate = (date) => {
    return date < moment(startDate) || date > moment("2023-06-03")
  }
  const RenderDropDownNavbars = () => {
    return (
      <div
        className={`flex gap-2 items-center flex-col py-4 px-4 bg-[rgb(194,194,194)] rounded-[10px] w-screen absolute top-[80px] left-0 z-50 xl:hidden over`}>
        <Tabs defaultActiveKey="1" centered >
          <Tabs.TabPane tab="Prediksi" key="1">
            <div className="ml-[20px] flex flex-col mt-5 gap-5 min-w-[280px]">
              <Select
                placeholder="Pilih Provinsi"
                options={Provinsi}
                onChange={(e) => setState({ ...state, province: e, kabupaten: null, stasiun: null, dataPrediksi: null})}
                value={province}
              />
              <Select
                placeholder="Pilih Kota/ Kabupaten"
                options={filterKabupaten}
                onChange={(e) => setState({ ...state, kabupaten: e, stasiun: null, dataPrediksi: null, })}
                value={kabupaten}
              />
              <Select
                placeholder="Pilih Stasiun"
                options={filterStasiun}
                onChange={(e) => setState({ ...state, stasiun: e, dataPrediksi: null})}
                value={stasiun}
              />
              <Tooltip title="Tanggal yang tersedia sebelum 2 Juli 2023" color="geekblue">

                <DatePicker
                  onChange={handleStarDate}
                  placeholder="Tanggal Awal"
                  disabledDate={handleDisableDate}
                  
                />
                <DatePicker
                  onChange={handleEndDate}
                  placeholder="Tanggal Akhir"
                  disabledDate={handleSesudahStartDate}
                />
             
                </Tooltip>
              <button className={clsx('bg-[#192841] text-white py-2 px-5 rounded-md', disabled && 'bg-gray-400 cursor-not-allowed')}
                disabled={disabled}
                onClick={handlePrediksi}
              >{
                  fetching ? <Spin /> : 'Prediksi'
                }</button>
            </div>

          </Tabs.TabPane >
          <Tabs.TabPane tab="Forecast" key="2">
            <div className="ml-[20px] flex flex-col mt-5 gap-5 min-w-[280px]">
              <Select
                placeholder="Pilih Provinsi"
                options={Provinsi}
                onChange={(e) => setState({ ...state, province: e, kabupaten: null, stasiun: null, dataPrediksi: null })}
                value={province}
              />
              <Select
                placeholder="Pilih Kota/ Kabupaten"
                options={filterKabupaten}
                onChange={(e) => setState({ ...state, kabupaten: e, stasiun: null, dataPrediksi: null})}
                value={kabupaten}
              />
              <Select
                placeholder="Pilih Stasiun"
                options={filterStasiun}
                onChange={(e) => setState({ ...state, stasiun: e, dataPrediksi: null})}
                value={stasiun}
              />
              <Slider min={0} max={100} onChange={handleChangeSlider} value={slider} />
              <button className={clsx('bg-[#192841] text-white py-2 px-5 rounded-md', disabledForcast && 'bg-gray-400 cursor-not-allowed')}
                disabled={disabledForcast}
                onClick={handleForecast}
              >{
                  fetching ? <Spin /> : 'Forecast'
                }</button>
            </div>

          </Tabs.TabPane>
        </Tabs>
      </div>
    )
  }
    return(
      
    <div className="flex w-full bg-[rgb(25,40,65,0.1)]">
        {/* main start */}
        {ModalParameters()}
      <div className="xl:w-[70%] w-full relative">
          <div className="h-[80px]  pl-[20px] flex items-center justify-between bg-[rgb(194,194,194,0.5)] cursor-pointer">
            <div className="flex gap-2 items-center">
              <RollbackOutlined className="bg-[#192841] text-white py-2 px-5 rounded-md" onClick={handleClikToHome} />
              <h1 className="ml-[20px] xl:text-4xl text-sm"> Prediksi Kebakaran Hutan di Indonesia</h1>
            </div>
            <button className="xl:hidden" onClick={() => setState({ ...state, showDropDown: !showDropDown })}>
                <svg className="w-6 h-6" viewBox="0 0 24 24">
                  <path fill="currentColor" d="M4 6h16v2H4zm0 5h16v2H4zm0 5h16v2H4z" />
                </svg>
            </button>
            {showDropDown && RenderDropDownNavbars()}
        </div>
          <Suspense fallback={<div>Loading...</div>}>
          <Carousel >
            <div className="flex justify-center !important ">
              <Card background="bg-bgCloud" className="snap-center " {...listDataParameter[0]} onClick={handleClickItemParameter.bind(null,listDataParameter[0])} />
              <Card background="bg-bgCloud" className="snap-center " {...listDataParameter[1]} onClick={handleClickItemParameter.bind(null,listDataParameter[1])} />
            </div>
            <div className="flex justify-center !important">
              <Card background="bg-bgCloud" className="snap-center " {...listDataParameter[2]} onClick={handleClickItemParameter.bind(null,listDataParameter[2])} />
              <Card background="bg-bgCloud" className="snap-center " {...listDataParameter[3]} onClick={handleClickItemParameter.bind(null,listDataParameter[3])} />
            </div>
            </Carousel>
          </Suspense>
        {/* chart and maps */}
          <div className="px-[20px]">
            <Tabs defaultActiveKey="1" centered >
              <Tabs.TabPane tab="Intensitas Kebakaran" key="1">
              <div className="p-[20px] rounded-md   bg-[rgb(194,194,194,0.5)]">
                  <h1 className="text-3xl text-center">Visualisasi Intensitas Kebakaran</h1>
                  <Suspense fallback={<div>Loading...</div>}>
                    <ChartMultiline rendah={rendah && rendah} label={tanggal && tanggal} sedang={sedang && sedang} tinggi={tinggi && tinggi} sangatTinggi={sangatTinggi && sangatTinggi} />
                  </Suspense>
              </div>
              </Tabs.TabPane>
              <Tabs.TabPane tab="FWI" key="2">
                <div className="p-[20px] rounded-md   bg-[rgb(194,194,194,0.5)]">
                  <h1 className="text-3xl text-center">Fire Weather Index</h1>
                  <Suspense fallback={<div>Loading...</div>}>
                    <Chart2 data={fwi[0]} tanggal={tanggal[0]} id="mychart2" />
                  </Suspense>
                </div>
              </Tabs.TabPane>
              <Tabs.TabPane tab="Maps" key="3">
                <div className="p-[20px] rounded-md  bg-[rgb(194,194,194,0.5)] my-5">
                  <Suspense key={state.dataPrediksi} fallback={<div>Loading...</div>}>
                    <MapGis height="400px" zoom={5} center={renderCenter()}>
                    <FeatureGroup>
                      {
                        (kabupaten && !fetching) && renderPoligon()
                      }
                      {
                          (kabupaten && !fetching) && renderMarker()
                      }
                      
                    </FeatureGroup>
                    </MapGis>
                  </Suspense>
                </div>
              </Tabs.TabPane>
              {!state.forecast && (
                <Tabs.TabPane tab="Error" key="4">
                  <div className="p-[20px] rounded-md  bg-[rgb(194,194,194,0.5)] my-5">
                    <Suspense fallback={<div>Loading...</div>}>
                      <BarChartV2 id="mychart3" data={error} />
                    </Suspense>
                  </div>
                </Tabs.TabPane>
              )}
            </Tabs>
        </div>

        </div>
        

      {/* main end */}
      {/* sidebar start */}
      <div className="xl:w-[30%] h-full xl:inline hidden">
        <div className=" bg-[rgb(194,194,194,0.5)] shadow-md xl:h-screen  rounded-md ">
          <div className="flex justify-center py-[10px]">
            <img src="/assets/bmkg.png" alt="bmkg" className="w-[50px]" />
            <div className="ml-[20px] flex flex-col">
              <h1 className="text-xl">{stasiun}</h1>
                <span className="text-sm text-transparent">Lang -0.343553 Long -0.4353535</span>
            </div>
          </div>
            <Tabs defaultActiveKey="1" centered >
              <Tabs.TabPane tab="Prediksi" key="1">
            <div className="ml-[20px] flex flex-col mt-5 gap-5">
                <Select
                placeholder="Pilih Provinsi"
                options={Provinsi}
                onChange={(e) => setState({ ...state, province: e, kabupaten:null, stasiun:null, dataPrediksi:null })}
                value={province}
                />
              <Select
                placeholder="Pilih Kota/ Kabupaten"
                options={filterKabupaten} 
                onChange={(e) => setState({ ...state, kabupaten: e, stasiun: null, dataPrediksi: null})}
                value={kabupaten}
                />
              <Select
                placeholder="Pilih Stasiun"
                options={filterStasiun}
                onChange={(e) => setState({ ...state, stasiun: e, dataPrediksi: null })}
                value={stasiun}
                  />
              <Tooltip title="Tanggal yang tersedia sebelum 2 Juli 2023" color="geekblue">
              <DatePicker.RangePicker 
                onChange={handleChangeDate}
                startDate={startDate}
                endDate={endDate}
                disabledDate={handleDisableDate}
                    />
                    </Tooltip>
              <button className={clsx('bg-[#192841] text-white py-2 px-5 rounded-md', disabled && 'bg-gray-400 cursor-not-allowed')}
                disabled={disabled}
                onClick={handlePrediksi}
              >{
                fetching ? <Spin /> : 'Prediksi'
                    }</button>
          </div>

                </Tabs.TabPane >
              <Tabs.TabPane tab="Forecast" key="2">
                <div className="ml-[20px] flex flex-col mt-5 gap-5">
                
                <Select
                  placeholder="Pilih Provinsi"
                  options={Provinsi}
                  onChange={(e) => setState({ ...state, province: e, kabupaten: null, stasiun: null, dataPrediksi: null})}
                  value={province}
                />
                <Select
                  placeholder="Pilih Kota/ Kabupaten"
                  options={filterKabupaten}
                  onChange={(e) => setState({ ...state, kabupaten: e, stasiun: null, dataPrediksi: null})}
                  value={kabupaten}
                />
                <Select
                  placeholder="Pilih Stasiun"
                  options={filterStasiun}
                  onChange={(e) => setState({ ...state, stasiun: e, dataPrediksi: null})}
                  value={stasiun}
                />
                  <Slider min={0} max={100} onChange={handleChangeSlider} value={slider} />
                  <button className={clsx('bg-[#192841] text-white py-2 px-5 rounded-md', disabledForcast && 'bg-gray-400 cursor-not-allowed')}
                    disabled={disabledForcast}
                    onClick={handleForecast}
                  >{
                      fetching ? <Spin /> : 'Forecast'
                    }</button>
          </div>

                </Tabs.TabPane>
              </Tabs>
        </div>
      </div>
      {/* sidebar end */}
    </div>
  );
}

export default Dashboard;