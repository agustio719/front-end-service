import React, { useEffect, useState } from "react";
import clsx from "clsx";
import capture1 from "../../assets/capture1.jpg"
import capture2 from "../../assets/capture2.jpg"
import capture3 from "../../assets/capture3.jpg"
import capture4 from "../../assets/capture4.jpg"
import capture5 from "../../assets/forecast.jpg"

function Component() {
  const [current, setCurrent] = useState(0);
  const [isNext, setIsNext] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setIsNext(true)
    }, 0)
  }, [current])
  const handleNext = () => {
    setIsNext(false)
    if (current === UserGuide.length - 1) {
      window.location.href = "/dashboard"
    }else {
      setTimeout(() => {
        setCurrent(current + 1)
      }, 1000);
    }
  }

  const handlePrev = () => {
    if (current === 0) {
      window.location.href = "/"
    } else {
      setIsNext(false)
      setCurrent(current - 1)
    }
  }

  const UserGuide = [
    {
      title: "Pilih menu 'Prediksi' atau 'Forecast'",
      description: "Menu berada di sidebar kanan",
      img: capture5
    },
    {
      title: "Pilih provinsi, kabupaten, dan stasiun",
      description: "Menu berada di sidebar kanan",
      img: capture1
    },
    {
      title: "Klik tombol 'Prediksi' untuk melihat hasil prediksi",
      description: "Tombol berada di sidebar kanan",
      img: capture2
    },
    {
      title: 'Klik gambar parameter untuk melihat detil parameter',
      description: 'Detil parameter berada di bagian atas halaman',
      img: capture3
    },
    {
      title: 'Klik Tabs untuk melihat hasilnya',
      description: 'Tabs berada di bagian tengah halaman',
      img: capture4
    }
  ]
  const handleSkip = () => {
    window.location.href = "/dashboard"
}
  return (
    <div className=" flex w-screen h-screen bg-white justify-center items-center bg-asap bg-cover bg-no-repeat">
      <div>
      <div className="flex justify-center items-center mb-2">
          <button className="bg-[#192841] text-white py-2 px-5 rounded-md" onClick={handleSkip}>Lewati</button>
          </div>
        <div className={clsx({
          "opacity-0 translate-x-[-100px] transition-all duration-1000 flex justify-center": !isNext,
          "opacity-100 translate-x-0 transition-all duration-500 flex justify-center": isNext,
        })}>
          {
          UserGuide[current] &&
            <div className=" xl:h-[500px] xl:w-[500px] w-[350px] h-[400px] shadow-md rounded-[10px] flex flex-col justify-center bg-gradient-to-r from-[rgba(25,40,65,0.8)] to-[rgba(25,40,65,0.5)]">
              <h1 className="xl:text-2xl text-xl font-bold text-center text-white">{UserGuide[current].title}</h1>
                <p className="xl:text-base text-sm text-center text-white">{UserGuide[current].description}</p>
                <div className="flex justify-center">
                  <img src={UserGuide[current].img} alt="capture" className="w-[300px] " />
                </div>
          </div>
          }
        </div>
        <div className="flex justify-between xl:w-[500px] w-[350px] mt-[20px]">
          <button className="bg-[#192841] text-white py-2 px-5 rounded-md" onClick={handlePrev}>Sebelumnya</button>
          <button className="bg-[#192841] text-white py-2 px-5 rounded-md" onClick={handleNext}>Selanjutnya</button>
        </div>
      </div>
      
    </div>
  )
}

export default Component