import React from "react";
function Card(props) {
  const { background, title, icon, className, bg, subtitle, onClick  } = props;
  return (
    <div className={`flex flex-wrap border-2 border-gray-200 rounded-lg p-4 ${background}  ${className} shadow-md bg-${bg} bg-cover bg-center xl:w-[450px] xl:h-[200px] w-[225px] h-[150px] cursor-pointer bg-[#5A5A5A] bg-blend-multiply`} onClick={onClick}>
      <div className="flex justify-between w-full items-center ">
        <div className="flex flex-col">
          <h1 className="xl:text-[32px] text-xl text-white font-mono">{title}</h1>
          <h1 className="xl:text-[24px] text-xl text-black font-bold">{subtitle}</h1>
        </div>
        <div className="flex flex-col">
          <div className=" bg-gray-200 rounded-full xl:w-[70px] xl:h-[70px] w-[30px] h-[30px] flex justify-center items-center">
            <img src={icon} alt="hot" className="xl:w-[50px] xl:h-[50px] w-[20px] h-[20px]" />
            </div>
        </div>
      </div>
    </div>
  );
}

export default Card;